#include <iostream>
using namespace std;
int main(){
    cout.setf(ios::fixed);
    cout.precision(4);

    int n, i=1;
    double r = 0.0000;
    cin >> n;

    while(n >= i){
        r = r + (1.0000/i);
        i++;
    }

    cout << r << endl;
}