#include <iostream>
#include <string>
using namespace std;
int main() {
    cout.setf(ios::fixed);
    cout.precision(4);

    int anios;
    double capital, interes;
    double p = 0.0000;
    string tipo; 
    
    cin >> capital >> interes >> anios >> tipo;
    
    if (tipo == "simple") {
        p = capital + (capital * (interes/100) * anios);
    } else if (tipo == "compost") {        
        p = capital + (capital * (interes/100));
        for (int i = 1; i < anios; i++) {
            p = p + (p * (interes/100));            
        }
    }
    
    cout << p << endl;
}