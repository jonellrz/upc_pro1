#include <iostream>
#include <cmath>
using namespace std;
int main() {
    cout.setf(ios::fixed);
    cout.precision(2);   
    double n, x, s1=0.00, s2=0.00;   
    cin >> n; 
    for (int i = 1; i <= n; i++) {
        cin >> x;
        s1 += pow(x, 2);
        s2 += x;
    }
    cout << (((1/(n-1)) * s1) - (1/(n*(n-1)) * pow(s2, 2))) << endl;
}