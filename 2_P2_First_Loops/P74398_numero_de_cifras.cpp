#include <iostream>
using namespace std;
int main() {
    int d, cont=0, base;
    cin >> d;
    int num = d;
    for (base=2; base < 17; base++) {
        if (d == 0) { cont++; }
        while (d > 0) {
            d /= base;
            cont++;
        }
        cout << "Base " << base << ": " << cont << " cifras." << endl;
        d=num;
        cont=0;
    }
}