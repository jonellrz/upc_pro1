#include <iostream>
using namespace std;
int main() {
    int d;
    cin >> d;
    if (d == 0) { cout << d; }
    while (d > 0)   {
        switch (d % 16) {
            case 10:
                cout << 'A';
                break;
            case 11:
                cout << 'B';
                break;
            case 12:
                cout << 'C';
                break;
            case 13:
                cout << 'D';
                break;
            case 14:
                cout << 'E';
                break;
            case 15:
                cout << 'F';
                break;
            default:
                cout << (d % 16);
                break;
        }
        d /= 16;
    }
    cout << endl;
}