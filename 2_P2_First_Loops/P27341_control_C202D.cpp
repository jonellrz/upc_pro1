#include <iostream>
using namespace std;
int main() {
    int a, b, total=0;
    while(cin >> a){
        cin >> b;
        for (int i = a; i <= b; i++) {
            total = (i*i*i) + total;     
        }
        cout << "suma dels cubs entre " << a << " i " << b << ": " << total << endl;
        total=0;
    }
}