#include <iostream>
using namespace std;
int main() {
    int dia, mes, anio, max_dias;
    
    while(cin >> dia >> mes >> anio){
        bool fc = false;
        //cin >> dia >> mes >> anio;
        switch(mes){
            case 4:
            case 6:
            case 9:
            case 11:
                max_dias = 30;
                break;
            case 2:
                max_dias = 28;
                if( ( anio%4==0 && anio%100!=0 ) || anio%400==0 ){ max_dias = 29; }
                break;        
            default:
                max_dias = 31;
                break;
        }
        if(anio >= 1800 && anio <= 9999) {
            if( mes >= 1 && mes <= 12 ) {
                if(dia >= 1 && dia <= max_dias) { fc = true; }
            }
        }
        if (fc) { cout << "Data Correcta" << endl; }
        else { cout << "Data Incorrecta" << endl; }
    }
}

/*

*/