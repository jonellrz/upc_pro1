#include <iostream>
using namespace std;

int factorial(int n);

int main(){
    int n;
    while(cin >> n){
        cout << factorial(n) << endl;
    }
    return 0;
}

int factorial(int n) {
    if(n<0 || n>12) return 0;
    int res = 1;
    for(int i=1; i<=n;i++){
        res *= i; 
    }    
    return res;
}