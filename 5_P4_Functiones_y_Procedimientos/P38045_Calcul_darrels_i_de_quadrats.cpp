#include <iostream>
#include <cmath>
using namespace std;

int main() {
    cout.setf(ios::fixed);
    cout.precision (6);
    int n;
    while (cin >> n) {
        double a=sqrt(n);
        cout << n*n << " " << a << endl;
    }
    return 0;
}