#include <iostream>
using namespace std;

bool es_any_de_traspas(int any){
    return (any%4==0 && (any%100!=0 || any % 400 ==0)) ? true : false;
}

int main() {
    int a;
    cin >> a;
    cout << (es_any_de_traspas(a)?"true":"false") << endl;

return 0;}