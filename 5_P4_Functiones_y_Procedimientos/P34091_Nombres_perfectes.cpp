#include <iostream>
using namespace std;

bool es_perfecte(int n);

int main() {
    int n;
    while(cin >> n) {
        cout << (es_perfecte(n)?"true":"false") << endl;
    }
    return 0;
}

bool es_perfecte(int n) {
    if (n == 1 || n == 0) {return false;}    
    int x = 1, i = 2;
    while (i*i < n && x <= n){
        if (n%i == 0) { x = x + i + n/i; }
        i = i + 1;
    }
    if (i*i == n){x = x + i;}
    return x == n;
}