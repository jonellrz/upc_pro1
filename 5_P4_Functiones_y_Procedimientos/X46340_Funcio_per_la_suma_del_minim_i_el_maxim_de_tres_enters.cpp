#include <iostream>
using namespace std;

int sum_min_max(int x, int y, int z) {
    int max = x;
    int min = x;

    if ( y > max) { max = y; }
    else if ( y < min) { min = y; }
    if ( z > max) { max = z; }
    else if ( z < min) { min = z; }

    return max + min;
}

int main(){
    int a, b, c;
    cin >> a >> b >> c;
    cout << sum_min_max(a, b, c) << endl;
return 0;}