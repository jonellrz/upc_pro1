#include <iostream>
using namespace std;

bool es_data_valida(int d, int m, int a) {
    int res = 0;
    int dia = 28;
    if(m>=1 && m<=12) { res++; }
    if(res==1 && a>=1800 && a<=9999) { res++; }

    // calcula los dias, teniendo en cuenta los dias del mes y los bisiesto
    if(res==2 && d>=1 && d<=31) { 
        switch(m){
            case 2: 
                if(a%4==0 && (a%100!=0 || a%400==0)) {dia = 29;}
                if(d>=1 && d<=dia) { res++; }
                break;
            case 1: case 3: case 5:
            case 7: case 8: case 10: case 12:
                if(d>=1 && d<=31) { res++; }
                break;
            case 4:  case 6:  case 9: case 11:
                if(d>=1 && d<=30) { res++; }
                break;
        }
    }
    if (res==3) { return true; }
    else { return false; }
}

int main() {
    int d, m, a;
    cin >> d >> m >> a;
    cout << (es_data_valida(d,m,a) ? "true":"false") << endl;
return 0;}