#include <iostream>
#include <cmath>
using namespace std;

int engreya(int x);

int main(){
    int n;
    while(cin >> n){
        cout << engreya(n) << endl;
    }
    return 0;
}
int engreya(int x){
    // contar digitos del numero
    int total_digitos=0;
    for(int i = x; i != 0; i=i/10) { total_digitos++; } 

    int num, tmp=0, res=0, expo;
    for(int i=x,j=1; i!=0; i=i/10,j++) {
        expo = pow( 10, (total_digitos-j) );
        num = (x/expo) % 10;
        if (tmp > num) {
            res += tmp*expo;
        } else {
            res += num*expo;
            tmp = num;
        }        
    }
    return res;
}