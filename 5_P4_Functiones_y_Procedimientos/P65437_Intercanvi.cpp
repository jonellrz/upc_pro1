#include <iostream>
using namespace std;

// definicion de methodos
void swap2(int& a, int& b) {
    int x = a;
    a = b;
    b = x;
}

// programa principal
int main() {
    int a, b;
    cin >> a >> b;
    swap2(a,b);
    cout << a << ' ' << b << endl;
return 0; }
