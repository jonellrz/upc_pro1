#include <iostream>
#include <string>
using namespace std;

string dia_de_la_setmana (int d, int m, int a) 
{
    int cl = (14 - m) / 12;
    int an = a - cl;
    int ms = m + 12 * cl - 2;
    int ds =  (d + an + an/4 - an/100 + an/400 + (31*ms)/12) % 7;
    if(ds == 0) { return "diumenge"; }
    else if(ds == 1) { return "dilluns"; }
    else if(ds == 2) { return "dimarts"; }
    else if(ds == 3) { return "dimecres"; }
    else if(ds == 4) { return "dijous"; }
    else if(ds == 5) { return "divendres"; }
    else { return "dissabte"; }
}

int main()
{
    int d, m, a;
    cin >> d >> m >> a;
    cout << dia_de_la_setmana(d,m,a) << endl;
    return 0;
}