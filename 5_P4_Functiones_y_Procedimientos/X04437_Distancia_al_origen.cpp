#include <iostream>
#include <cmath>
using namespace std;


// calcula las coordenadas
double dist_or(double x, double y){
    return sqrt(pow(x, 2.00) + pow(y, 2.00));
}

// programa principal
int main() {
    double x, y;
    cin >> x >> y;
    cout << dist_or(x, y) << endl;
return 0; }