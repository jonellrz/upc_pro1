#include <iostream>
using namespace std;
int nombre_digits(int n);

int main() {
    int n;
    while(cin >> n){ cout << nombre_digits(n) << endl; }
    return 0;
}

int nombre_digits(int n) {
    int num=0;
    for(int i=n; i > 0; i=i/10) { num++; }
    if(n==0) {num=1;}
    return num;
}