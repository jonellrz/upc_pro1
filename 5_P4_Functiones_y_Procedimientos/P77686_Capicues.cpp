#include <iostream>
using namespace std;

bool es_capicua(int n);

int main() {
    int n;
    while(cin >> n) {
        cout << (es_capicua(n)?"true":"false") << endl;
    }
    return 0;
}

bool es_capicua(int n) {
    int rev=0;
    // invertimos el numero 
    for(int i=n; i > 0; i=i/10){ rev = rev*10 + i%10; }
    return (rev == n) ? true : false;
}