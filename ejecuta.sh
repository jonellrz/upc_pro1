#!/bin/bash
CARPETA='bin'
TIPO='bin'
NOMBRE_REAL=${1}
ARCHIVO="${NOMBRE_REAL##*/}"
ARCHIVO="${ARCHIVO%.*}"
RECOMPILA=${2}

# opcional
#clear
###
echo -e "==========================================================="
## prepara entorno
if [ ! -d "${CARPETA}" ]; then
    echo "| > Primera vez que se usa el programa, preparando entorno en carpeta: ${CARPETA}"
    mkdir ${CARPETA}
fi

## compila programa
RC_ARCHIVO="${CARPETA}/${ARCHIVO}.${TIPO}";
if [ ! -f "${RC_ARCHIVO}" ] || [ "${RECOMPILA}" == "s" ]; then
    echo "| > Compilando ${NOMBRE_REAL} en: ${RC_ARCHIVO}"
    g++ "${NOMBRE_REAL}" -o "${RC_ARCHIVO}"

    echo "| > Otorgando permisos de ejecución +x a: ${RC_ARCHIVO}";
    chmod +x "${RC_ARCHIVO}";
fi

## ejecuta programa
if [ -f "${RC_ARCHIVO}" ]; then
    echo "| > Ejecutando: ${RC_ARCHIVO}";
    echo -e "-----------------------------------------------------\n";
    "${RC_ARCHIVO}"
fi
echo -e "\n==========================================================="