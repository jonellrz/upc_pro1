#include <iostream>
using namespace std;
int main() {
    int n, m, cm, num=9, salto=0;
    while(cin >> n >> m){
        if(salto > 0){ cout << endl; }
        num = 9;
        cm = m;
        while(n--){
            m = cm;
            while(m--){ 
                cout << num;
                if(num <= 0) {
                    num = 9;
                } else {
                    num--;
                }
            }
            cout << endl;
        }
        salto++;
    }
}