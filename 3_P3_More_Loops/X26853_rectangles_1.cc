#include <iostream>
using namespace std;
int main() {
    int n, m, cm, mod, salto=0;
    while(cin >> n >> m){
        if(salto > 0){ cout << endl; }
        mod = (n+m)%5;
        cm = m;
        while(n--){
            m = cm;
            while(m--){ cout << mod; }
            cout << endl;
        }
        salto++;
    }
}