#include <iostream>
using namespace std;
int main() {
    int base, numero, contador;
    while(cin >> base >> numero){
        contador=0;
        while(numero != 0){
            numero /= base;
            contador++;
        }
        cout << contador << endl;
    }
}