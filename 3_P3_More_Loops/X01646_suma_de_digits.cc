#include <iostream>
using namespace std;
int main() {
    int base, numero, suma, operar;
    cin >> base;
    while(cin >> numero){
        suma=0;
        operar = numero; // copia el numero para no perder el valor
        while(operar > 0){
            suma += operar%base;
            operar = operar/base;
        }
        cout << numero << ": " << suma << endl;
    }
}