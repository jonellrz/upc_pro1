#include <iostream>
using namespace std;
int main() {
    int num, cuenta=0;
    while(cin >> num){
        cuenta = 0;
        while(num != 1){
            if(num%2 == 0){
                num = num / 2;
            } else if (num%2 != 0) {
                num = (num * 3) + 1;
            }
            cuenta++;
        }
        cout << cuenta << endl;
    }
}