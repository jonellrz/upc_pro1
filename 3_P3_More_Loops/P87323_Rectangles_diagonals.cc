#include <iostream>
using namespace std;
int main() {
    int fil, col, ccol;
    cin >> fil >> col;
    for (int i = 1; i <= fil; i++){
        ccol = 1;
        for (int j = 1; j <= col; j++){
            if(i >= j) {
                cout << (i-j) % 10;
            } else {
                cout << ccol % 10;
                ccol++;
            }
        }
        cout << endl;
    }
}