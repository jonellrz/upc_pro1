#include <iostream>
using namespace std;
int main() {
    int fil, col, cpcol, suma=0;
    long numero;
    cin >> fil >> col;
    bool pcol = (col%2==0&&col!=1) ? false : true, pfil=false;
    while(fil--){
        //cout << ">> "<< " suma:"<<suma << " pcol:"<<pcol << " pfil:"<<pfil << " col:"<<col << endl;
        cpcol = col;
        cin >> numero;
        while(cpcol--){
            if(pcol) { 
                suma += numero % 10;
                //cout << "- suma:"<<suma << " n:"<< numero << " n10:"<< numero%10 << endl;
            }
            numero = numero / 10;
            pcol = pcol ? false : true;
        }
        pfil = pfil ? false : true;
        //cout << "<< "<< " suma:"<<suma << " pcol:"<<pcol << " pfil:"<<pfil << " col:"<<col << endl;
        if(col==1){
            pcol = !pfil ? true : false;
        } else if (col%2==0) {
            pcol = pfil ? true : false;
        } else if (col%2!=0) {
            pcol = pfil ? false : true;
        }
    }
    cout << suma << endl;
}