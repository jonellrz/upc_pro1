#include <iostream>
using namespace std;
int main() {
    int numero, mult, mod, operar;
    while(cin >> numero){
        do {
            mult=(numero==0)?0:1;
            operar = numero; // copia el numero para no perder el valor
            while(operar > 0){
                mod = operar%10;
                mult = mod * mult;
                operar = operar/10;
            }
            cout << "El producte dels digits de "<< numero << " es " << mult << "." << endl;
            numero = mult;
        } while(numero>9);
        cout << "----------" << endl;
    }
}