#include <iostream>
#include <cmath>
using namespace std;
int main() {
  cout.setf(ios::fixed);
  cout.precision(4);
  double polinomi, polic, res = 0;
  int i = 0;
  cin >> polinomi;
  while (cin >> polic){
    res += polic * pow(polinomi, i++);
  }
  cout << res << endl;

}