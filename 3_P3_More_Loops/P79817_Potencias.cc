#include <iostream>
using namespace std;
int main() {
    int base, potencia, res;
    while(cin >> base >> potencia) {
        res = base;
        if(potencia == 0){
            res = 1;
        } else if (base == 0) {
            res = 0;
        } else {
            while(--potencia){
                res *= base;
            }
        }
        cout << res << endl;
    }
}