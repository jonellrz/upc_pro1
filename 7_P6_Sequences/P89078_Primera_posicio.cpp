#include <iostream>

using namespace std;

int main () {
    int num, pos, cnt=1;
    do {
        cin >> num;
        if(num%2 == 0) {
            pos = cnt;
        }
        cnt++;
    } while(num%2 != 0);
    cout << pos << endl;
    return 0;
}
