#include <iostream>

using namespace std;
int rotacio_dreta(int x, int k);
int main () {
    int num, mov;
    while(cin >> num >> mov) {
        cout << rotacio_dreta(num, mov) << endl;
    }
    return 0;
}

int rotacio_dreta(int x, int k){

    // el valor posicional maximo del numero
    int peso = 1;
    for(int i=x; i > 9 ;i/=10){ peso *= 10; }

    // gira numero
    for(int i=0; i < k; i++){
        x = x/10 + peso*(x%10);
    }
    return x;
}