#include <iostream>

using namespace std;
int main(){
    int a, b;
    int cnt=1;
    while(cin >> a >> b) {
        if (a%b != 0) { a = b*((a/b)+1); }
        cout << '#' << cnt << " : " << a << endl;
        cnt++;
    }
    return 0;
}