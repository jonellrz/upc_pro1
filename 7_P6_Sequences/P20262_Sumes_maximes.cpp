#include <iostream>
#include <vector>

using namespace std;
int main() {
    int tot;
    while(cin >> tot){
        vector<int> numeros(tot);
        int d_suma = 0;
        int i_suma = 0;
        int d_maximo = 0;
        int i_maximo = 0;

        /**
         * rellena el vector
         */
        for(int v=0; v<tot; v++) { cin >> numeros[v]; }

        /**
         * suma y remplaza el maximo, 
         * recorriendo el vector de izq a der
         * y de der a izq
         */
        for(int d=0, i=(tot-1); d<tot; d++, i--){
            d_suma += numeros[d];
            i_suma += numeros[i];
            d_maximo = (d_suma > d_maximo) ? d_suma : d_maximo;
            i_maximo = (i_suma > i_maximo) ? i_suma : i_maximo;
        }


        cout << d_maximo << " " << i_maximo << endl;
    }

}