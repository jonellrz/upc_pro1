#include <iostream>

using namespace std;
int main () {
    char letra, t='n';

    while(cin >> letra && letra != '.') {
        t = (letra == 'a') ? 's' : t;
    }
    
    cout << ((t == 's') ? "si" : "no") << endl;  

    return 0;
}