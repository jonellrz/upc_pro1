#include <iostream>
#include <string>
using namespace std;
 
 
void voltear(int& total) {
    string palabra;
    if (cin >> palabra) {
        total++;
        voltear(total);
        if (total <= 0) {
            cout << palabra << endl;
        }
        total -= 2;
    }
}
 
int main() {
    int total = 0;
    voltear(total);
}