#include <iostream>
using namespace std;

void barras(int num);

int main(){
    int num;
    cin >> num;
    barras(num);
    return 0;
}

void barras(int num) {
    if(num <= 1) {
        cout << '*' << endl;
    } else {
        barras(num-1);
        for (int i = 0; i != num; ++i) cout << '*';
        cout << endl;
        barras(num-1);
    }
}