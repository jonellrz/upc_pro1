#include <iostream>
#include <string>
using namespace std;
 
void digit_maxim_i_minim(int num, int& maxim, int& minim) {
    if(num > 0) {
        int tmp = num%10;
        if(tmp>maxim) maxim = tmp;
        if(tmp<minim) minim = tmp;
        digit_maxim_i_minim(num/10, maxim, minim);
    }
}
 
int main() {
    int num;
    while (cin >> num) {
        int maxim=0, minim=num;
        digit_maxim_i_minim(num, maxim, minim);
        cout << num << ' ' << maxim << ' ' << minim << endl;
    }
}
