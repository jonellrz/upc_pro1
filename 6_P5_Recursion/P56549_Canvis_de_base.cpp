#include <iostream>
#include <string>
using namespace std;

// creamos 3 funciones para tener el cambio de base
void base_cambio(int num, int base);

int main(){
    int num;
    while(cin >> num) {
        cout << num << " = ";
        base_cambio(num, 2);
        cout << ", ";
        base_cambio(num, 8);
        cout << ", ";
        base_cambio(num, 16);
        cout << endl;
    }
    return 0;
}

void base_cambio(int num, int base) {
    int tmp = num / base;
    if ( tmp != 0 ) base_cambio(tmp, base);
    if ( (num-base*tmp) >= 10) {
        char k = 'A' + (num%16) - 10;
        cout << k;    
    } else {
        cout << num-base*tmp; 
    }
}
