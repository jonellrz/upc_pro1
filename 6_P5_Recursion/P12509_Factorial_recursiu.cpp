#include <iostream>
using namespace std;

int factorial(int n);

int main(){
    int num = 5;
    cin >> num;
    cout << factorial(num) << endl;
    return 0;
}

int factorial(int n) {
    return (n <= 1) ? 1 : n*factorial(n-1);
}