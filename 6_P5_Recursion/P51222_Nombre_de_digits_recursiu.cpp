#include <iostream>
using namespace std;

int nombre_digits(int n);

int main(){
    int n;
    while(cin >> n) {
        cout << nombre_digits(n) << endl;
    }
    return 0;
}

int nombre_digits(int n) {
    return (n <= 9) ?  1 : 1+nombre_digits(n/10);
}