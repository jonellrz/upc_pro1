#include <iostream>
#include <string>
using namespace std;
 
 
void voltear(int& n) {
    string palabra;
    if (cin >> palabra) {
        voltear(n);
        if (n != 0) {
            cout << palabra << endl;
            --n;
        }
    }
}
 
int main() {
    int n;
    cin >> n;
    voltear(n);
}