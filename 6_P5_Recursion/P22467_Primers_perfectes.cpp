#include <iostream>
using namespace std;

bool es_primer_perfecte(int n) {
    // validamos que el numero sea primo
    if(n <= 1) { return false; }
    for(int i=2; i*i <= n; ++i) {
        if(n%i == 0) return false;
    }

    // si llegamos a este punto, el numero es primo
    // sumar sus cifras
    int suma = 0;
    for(int i=n;i>0;i/=10) { suma += i%10; }

    if (suma >= 10) {
        
        es_primer_perfecte(suma); 

    } else if ( suma <= 9 && (suma == 2 || suma%2!=0) ) {
        cout << "suma: " << suma << endl;
        return true;
    } else {
        return false;
    }
    return false;
}

int main() {
    int num;
    while (cin >> num) {
        cout << es_primer_perfecte(num) << endl;
    }
}