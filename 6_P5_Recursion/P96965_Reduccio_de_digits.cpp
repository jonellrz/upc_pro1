#include <iostream>
using namespace std;

int reduccio_digits(int x);
int suma_digits(int x);

int main(){
    int n;
    while(cin >> n) {
        cout << reduccio_digits(n) << endl;
    }
    return 0;
}


int reduccio_digits(int x) {
    int num = suma_digits(x);
    return (num <= 9) ? num : reduccio_digits(num);
}

int suma_digits(int x) {
    return (x < 10) ? x : suma_digits(x/10) + x%10;
}