#include <iostream>
using namespace std;
int main() {
    // FALLA en privados del jutge
    int hor, min, contador;
    while(cin >> hor >> min){
        contador = 0;
        hor = (hor > 12) ? hor-12 : (hor==0) ? 12 : hor;
        contador += hor+1;
        if(min <= 15) { contador += 1; }
        if(min <= 30) { contador += 1; }
        if(min <= 45) { contador += 1; }
        if(min == 0) { contador = hor; }
        min = 60-min;
        min = min == 60 ? 0 : min;
        cout << min << ' ' << contador << endl;
    }
}
// para estos casos
// 0 15 -> 90 7
// 13 0 -> 60 6