#include <iostream>
using namespace std;
int main() {
    // entradas
    int a1, b1, a2, b2, x, y;
    char sbl = '?';
    // lee pantalla
    cin >> a1 >> b1 >> a2 >> b2; 
    // opera
    if(a2 > a1) {
        x = a2;
    } else {
        x = a1;
    }
    if(b2 < b1) {
        y = b2;
    } else {
        y = b1;
    }
    if (a1 == a2 && b1 == b2) {
        sbl = '=';
    } else if ( (a1 >= a2 && b2 > b1) || (a1 > a2 && b2 >= b1) ){
        sbl = '1';
    } else if ( (a2 >= a1 && b1 > b2) || (a2 > a1 && b1 >= b2) ) {
        sbl = '2';
    }
    // Salida
    if (x > y){
        cout << sbl << " , " << "[]" << endl;
    } else {
        cout << sbl << " , " << "[" << x << "," << y << "]" << endl;
    }
}