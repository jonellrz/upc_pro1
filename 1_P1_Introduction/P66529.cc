#include <iostream>
#include <string>
using namespace std;
int main() {
    cout.setf(ios::fixed);
    cout.precision(4);

    double interes;
    string tipo_interes;
    int ciclo;
    cin >> interes >> tipo_interes;
    interes = interes/100;
    double interes_acumulado = 1;

    if(tipo_interes == "semestral") {
        ciclo = 2;
    } else if (tipo_interes == "trimestral") {
        ciclo = 4;
    } else if (tipo_interes == "mensual") {
        ciclo = 12;
    } else if (tipo_interes == "setmanal") {
        ciclo = 52;
    }

    int vuelta = ciclo;
    while(vuelta--){
        interes_acumulado = interes_acumulado*(1+(interes/ciclo));
    }

    cout << (interes_acumulado-1)*100 << endl;

}