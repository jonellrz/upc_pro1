#include <iostream>
using namespace std;
int main(){

    // entradas
    int a1, b1, a2, b2, x, y;

    // lee pantalla
    cin >> a1 >> b1 >> a2 >> b2;

    // opera
    x = (a2 > a1) ? a2 : a1;    
    y = (b2 < b1) ? b2 : b1;

    // Salida
    if (x > y){
        cout << "[]" << endl;
    } else {
        cout << "[" << x << "," << y << "]" << endl;
    }
}