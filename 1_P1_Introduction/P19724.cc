#include <iostream>
using namespace std;
// Funciona
int main() {
    int hor, min, tiempo, contador;
    while(cin >> hor >> min >> tiempo){
        contador = 0;
        while(tiempo--){
            if(min == 0) {
                contador += 4;
                if(hor == 12) { contador += 100; 
                } else if (hor==0) { contador += 12; 
                } else if (hor<12) { contador += hor; 
                } else { contador += hor - 12; }
            } else if(min == 15) { contador += 1;
            } else if(min == 30) { contador += 2;
            } else if(min == 45) { contador += 3; }
            if (++min == 60) {
                min = 0;
                if (++hor == 24) {
                    hor = 0;
                    contador += 484*(tiempo/1440);
                    tiempo = tiempo % 1440;
                }
            }
        }
        cout << contador << endl;
    }
}