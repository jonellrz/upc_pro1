#include <iostream>
using namespace std;
int main(){

    int h,m,s;
    int sm = 0, sh = 0;

    cin >> h >> m >> s;

    if( (s+1) >= 60) {
        s = 0;
        sm = 1;
    } else { s += 1; }

    if( (m+sm) >= 60) {
        m = 0;
        sh = 1;
    } else { m += sm; }
    
    if( (h+sh) >= 24) { h = 0; } else { h += sh; }

    if (h < 10) { cout << "0" << h; } else { cout << h; }
    cout << ":";
    if (m < 10) { cout << "0" << m; } else { cout << m; }
    cout << ":";
    if (s < 10) { cout << "0" << s; } else { cout << s; }

    cout << endl;
}